#!/bin/bash

# create new xephyr window
Xephyr -resizeable -ac :42 &
# export display to new window
export DISPLAY=:42
# start xcfe4 in this new x
startxfce4
# reset display to 0
export DISPLAY=:0
