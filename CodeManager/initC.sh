#!/bin/zsh

# welcome
welcome='
   ******           **   ****     **   **   **********
  **////**         /**  /**/**   /**  /**  /////**/// 
 **    //          /**  /**//**  /**  /**      /**    
/**                /**  /** //** /**  /**      /**    
/**                /**  /**  //**/**  /**      /**    
//**    **   **    /**  /**   //****  /**      /**    
 //******   /**    /**  /**    //***  /**      /**    
  //////    //     //   //      ///   //       //
                   by phineas0fog                   \n';

# check if lolcat is installed
which lolcat > /dev/null;
retCode=$?;
if [ $retCode -eq 0 ]
then
    echo $welcome | lolcat;
else
    echo $welcome;
fi


function libInclude() {
    libs=000;
    echo "Include string.h ? [y/N]";
    yn='';
    read yn;
    case $yn in
	[Yy]*)
	    libs=$(($libs+1));
	    ;;
	* ) ;;
    esac
    echo "Include math.h ? [y/N]";
    yn='';
    read yn;
    case $yn in
	[Yy]*)
	    libs=$(($libs+10));
	    ;;
	* ) ;;
    esac
    echo "Include time.h ? [y/N]";
    yn='';
    read yn;
    case $yn in
	[Yy]*)
	    libs=$(($libs+100));
	    ;;
	* ) ;;
    esac
    return libs;
}

# core
echo "Enter the project's name...";
read projectName;
echo "Add decription ? [y/N]";
yn='';
read yn
case $yn in
    [Yy]*) echo "Enter description...";
	   read description;
	   ;;
    * ) echo -e "\033[1;32mNo description added\033[0m"; ;;
esac
echo "Include more libraries ? [y/N]";
yn='';
read yn;
case $yn in
    [Yy]*) libInclude; libs=$?; ;;
    * ) echo -e "\033[1;32mNo more libs included\033[0m"; ;;
esac


# add description if needed
if [ $description != '' ]
then
    echo -e "/* ***** DESCRIPTION ***** */" > header.h;
    echo -e "/* $description */" >> header.h;
    echo -e "\n" >> header.h;
fi

echo -e "/* ******* LICENSE ******* */" >> header.h;
echo -e "/* Created by \"phineas0fog\" e.vanespen[at]protonmail.com*/" >> header.h;
echo -e "/* And placed under CC BY-NC-SA license */" >> header.h;

echo -e  "\033[1;32mIncluding libraries...\033[0m";
# include libraries
echo -e "#include <stdio.h>\n#include <stdlib.h>" >> header.h;
if [[ $libs =~ '1..' ]]; then
    echo -e "#include <string.h>" >> header.h;
fi
if [[ $libs =~ '.1.' ]]; then
    echo -e "#include <math.h>" >> header.h;
fi
if [[ $libs =~ '..1' ]]; then
    echo -e "#include <time.h>" >> header.h;
fi

# include personnal files
echo -e "\n#include \"phineas.c\"" >> header.h;

echo -e  "\033[1;32mDone\033[0m";

# create and fill c files
echo -e  "\033[1;32mCreate and fill c files...\033[0m";

echo -e "#include \"header.h\"" > functions.c;
echo -e "#include \"header.h\"" > main.c;
echo -e "#define DEBUG 1" >> main.c;
echo -e "int main(void){\n\n}" >> main.c;
echo -e  "\033[1;32mDone\033[0m";

# making src and build repertories and moves the files in src
echo -e "";
echo -e  "\033[1;32mMake src and build directories...\033[0m";
mkdir src build;
mv header.h functions.c main.c src;
echo -e  "\033[1;32mDone\033[0m";

