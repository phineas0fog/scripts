#!/bin/zsh

welcome='
*******      *******     **       **   ********   *******  
/**////**    **/////**   /**      /**  /**/////   /**////** 
/**   /**   **     //**  /**   *  /**  /**        /**   /** 
/*******   /**      /**  /**  *** /**  /*******   /*******  
/**////    /**      /**  /** **/**/**  /**////    /**///**  
/**        //**     **   /**** //****  /**        /**  //** 
/**         //*******    /**/   ///**  /********  /**   //**
//           ///////     //       //   ////////   //     //*

         ******      *******     *******     ********
        **////**    **/////**   /**////**   /**///// 
       **    //    **     //**  /**    /**  /**      
      /**         /**      /**  /**    /**  /******* 
      /**         /**      /**  /**    /**  /**////  
      //**    **  //**     **   /**    **   /**      
       //******    //*******    /*******    /********
        //////      ///////     ///////     ////////
                      by phineas0fog                       \n';

# check if lolcat is installed
if [ $1 = "-nologo" ]
then
    echo "POWER CODE utility v0.1 by phineas0fog...\n";
else
    clear;
    which lolcat > /dev/null;
    retCode=$?;
    if [ $retCode -eq 0 ]
    then
	echo $welcome | lolcat;
    else
	echo $welcome;
    fi
fi

echo -e "\033[1;33mWelcome in the POWER CODE utility v0.1\033[0m";
echo -e "\033[1;33m--------------------------------------\033[0m";
echo -e "Project";
echo -e "\t1.\tInit a new project";
echo -e "\t2.\tBuild project";
echo -e "\n";
echo -e "--------------------------------------";
echo -e "\t9.\tQuit";
read choice;
case $choice in
    1)
	echo -e "Language ?";
	echo -e "\t1.\tC";
	echo -e "\t2.\tJava";
	choice=''; read choice;
	case $choice in
	    1) clear; ./initC.sh; ;;
	    2) echo -e "\033[1;31mNot implemented yet !\033[0m"; return 0; ;;
	    *) echo -e "\033[1;31mLeaving...\033[0m"; sleep 0.5; return 0; ;;
	esac
    ;;
    2) ;;
    *) echo -e "\033[1;31mLeaving...\033[0m"; return 0; ;;
esac
