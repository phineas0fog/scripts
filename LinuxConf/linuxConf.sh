#!/bin/bash

fileName=$1;
extension="${fileName##*.}"
RED="\033[1;31m";
GREEN="\033[1;32m";
BLUE="\033[1;36m";
BOLD="\033[1m";
NOR="\033[0m";


if [ $# -eq 0 ]
then
    echo "You must specify file";
fi

if [ $extension != "json" ]
then
    echo "You must specify JSON file";
fi

length=$(jshon -l < $fileName);

if [ $# -eq 1 ]
then
    current=0;
else
    current=$2;
fi

while [ $current -lt $length ]
do
    clear;
    command=$(jshon -e $current < $fileName | jq .command);  
    description=$(jshon -e $current < $fileName | jq .description);

    echo -e "Command $RED$(($current+1))$NOR/$GREEN$length$NOR";
    echo -e "\t$BOLD$command$NOR" | tr -d '"';
    echo -e "\t$description" | tr -d '"';
    
    current=$(($current + 1));
    read;
done
