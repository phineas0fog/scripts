#!/bin/zsh

#get owner of parent dir
parentDirOwner=$(stat -c %U .);

#get current user
user=$(whoami);

if [ $parentDirOwner != $user ]
then
    echo -e '\033[1;31m Using sudo ? (y/n) \033[0m';
    read yn;
    case $yn in
	[Yy]*)
	    if [ -d $1 ]
	    then
		sudo cp -r $*;
	    else
		sudo cp $*;
	    fi
	    ;;
	[Nn]*) echo -e '\033[1;31m Aborted ! \033[0m'; exit 1; ;;
	* ) echo -e '\033[1;31m Error, exit. \033[0m'; exit 1; ;;
    esac
else
    if [ -d $1 ]
    then
	cp -r $*;
    else
	cp $*;
    fi
fi
