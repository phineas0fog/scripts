#!/bin/bash

# supported files types
txt=(txt conf ini cfg sh org md log markdown asc );
img=(png jpg jpeg PNG JPG JPEG bmp );
pdf=(pdf);
mov=(avi mp4 mkv);

# readers
txtR="cat";
imgR="viewnior";
pdfR="qpdfview";
movR="mpv";

# add array of file formats
# <arrayName>=(<list of formats>);

# add reader
# <arrayName>R=<command to view file>;

# vars
file=$1;
extension=${file##*.};

# print error if there are more than ONE argument
if [ $# -gt 1 ]
then
    echo -e '\033[1;31m Only ONE file can be visualized for the moment.... \033[0m';
fi

# core
if [ -d $1 ]
then
    ls $1;
elif [ ! -d $1 ] && [ -f $1 ]
then
    # for plain text files
    for elem in ${txt[@]}
    do
	if [ $elem = $extension ]
	then
	    $txtR $1;
	fi
    done
    # for pictures
    for elem in ${img[@]}
    do
	if [ $elem = $extension ]
	then
	    $imgR $1;
	fi
    done
    # for movies
    for elem in ${mov[@]}
    do
	if [ $elem = $extension ]
	then
	    $movR $1;
	fi
    done
    # for pdf
    for elem in ${pdf[@]}
    do
	if [ $elem = $extension ]
	then
	    $pdfR $1;
	fi
    done

    # to customize
    # for elem in <extensions array>
    # do
    #    if [ $elem = $extension ]
    #    then
    #        <correct reader variable> $1;
    #    fi
    # done
    
fi
