#!/bin/zsh

i=0;
if [ $1 = 0 ]
then
    for i in 1 2 3 4 5
    do
	msi-keyboard -m normal -r front-left,0,255,0;
	sleep 0.05;
	msi-keyboard -m normal -r front-left,0,0,0;
	sleep 0.1;
    done
elif [ $1 = -1 ]
    then
	for i in 1 2 3 4 5
	do
	    msi-keyboard -m normal -r front-left,255,00,255;
	    sleep 0.05;
	    msi-keyboard -m normal -r front-left,0,0,0;
	    sleep 0.1;
	done
else
    for i in 1 2 3 4 5
    do
	msi-keyboard -m normal -r front-left,255,0,0;
	sleep 0.05;
	msi-keyboard -m normal -r front-left,0,0,0;
	sleep 0.1;
    done
fi
    
