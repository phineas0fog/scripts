#!/bin/zsh

if [ $# -eq 0 ]
then
    ls --color=tty .
elif [ -d $1 ]
then
     ls --color=tty $1;
elif [ -f $1 ]
then
    cat $1;
else
    echo -e '\033[1;31m Not a dir or file. \033[0m';
fi
