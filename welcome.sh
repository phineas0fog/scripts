#!/bin/bash

updates=$(checkupdates | wc -l);
AURupdates=$(cower -u | wc -l);

echo -e "\033[1;32mWelcome $(whoami) \033[0m";
echo -e "\033[1;32m     ---------     \033[0m";
echo -e "You have currently $updates from Arch official repos and\n
     	$AURupdates from Arch User Repo";
