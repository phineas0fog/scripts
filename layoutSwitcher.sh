#!/bin/bash

# Usage :
#    -s : switch between layouts
#    -g : get current layout

OPTS=$(getopt -o s,g --)

# get current layout
curLay=$(setxkbmap -query | grep layout | tr -s " " "X" | cut -d "X" -f 2);

# define layouts
lay1="fr";
lay2="us";

while true
do
    case "$1" in
	-s)
	    # switch
	    if [ "$curLay" == "$lay2" ]
	    then
		setxkbmap $lay1;
	    else
		setxkbmap $lay2;
	    fi
	    break;
	    ;;
	-g)
	    echo $curLay;
	    break;
	    ;;
	*)
	    echo -e "Usage :\n\t-s : switch between layouts\n\t-g : get current layout";
	    break;
	    ;;
    esac
done
	
	 
