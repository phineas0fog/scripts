#!/bin/bash

echo "Killing old instances...";
killall emacs;
if [ $? = 0 ]
then
	echo "Old instances killed";
else
	echo "No old instances";
fi
echo "Starting deamon";
emacs --daemon;
if [ $? != 0 ]
then
	echo "DAEMON START FAIL";
else
	echo "Daemon started succefully";
fi
