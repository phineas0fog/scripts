#!/bin/env sh

#init counter
i=0;

#check if buffer exist
if [ ! -f /tmp/buffer ]
then
    echo "Nothing to paste...";
    exit 1;
fi

#get paths in buffer file and paste in the current repertory
while [ $i -lt $(head -1 /tmp/buffer) ]
do
    echo Copying $(sed -n "$(($i+2))"p /tmp/buffer) in $(pwd);
    cp -r $(sed -n "$(($i+2))"p /tmp/buffer) .;
    let i++;
done

#delete the now useless buffer file
rm /tmp/buffer;
