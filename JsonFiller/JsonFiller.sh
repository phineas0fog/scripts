#!/bin/bash

# VARIABLES
## Welcome message
welcome="
     @@@  @@@@@@  @@@@@@  @@@  @@@
     @@! !@@     @@!  @@@ @@!@!@@@
     !!@  !@@!!  @!@  !@! @!@@!!@!
 .  .!!      !:! !!:  !!! !!:  !!!
 ::.::   ::.: :   : :. :  ::    : 
                                  
 @@@@@@@@ @@@ @@@      @@@      @@@@@@@@ @@@@@@@ 
 @@!      @@! @@!      @@!      @@!      @@!  @@@
 @!!!:!   !!@ @!!      @!!      @!!!:!   @!@!!@! 
 !!:      !!: !!:      !!:      !!:      !!: :!! 
  :       :   : ::.: : : ::.: : : :: :::  :   : :
                 by phineas0fog
";

## Colors
RED="\033[1;31m";
GREEN="\033[1;32m";
BLUE="\033[1;36m";
BOLD="\033[1m";
NOR="\033[0m";

# Display welcome message
echo -e "$welcome" | lolcat;

# Core
echo -e $BLUE"Enter the desired name for file... $NOR";
read fileName;
if [ -f $fileName ] || [ -f $fileName.json ]
then
    echo -e $RED"File already exist ! Exiting... $NOR";
    exit 42;
else
    touch $fileName.json;
    echo -e $GREEN"File created ! $NOR";
fi

echo -e $BLUE"Enter the desired names for fields... (Ctrl+C Return to quit) $NOR";
i=1;
field[1]='';
while true
do
    echo "Field $i :";
    read field[$i];
    i=$(($i+1));
    trap 'break' SIGINT;
done

echo $field[$i];

echo -e $BOLD"----------"$NOR;

echo -e $BLUE"Now, fill the fields"$NOR;

while true
do
    for j in $(seq 1 $i)
    do
	echo "Fill " $field[$j];
	break;
    done
    break;
done

