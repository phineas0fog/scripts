#!/bin/bash

# check if docker is installed
which docker > /dev/null;
if [ $? != 0 ]
then
    echo -e "\033[1;31mDocker is not installed... Exiting...\033[0m";
fi

# first run
echo -e "Start SQL serveur...";
docker run -d -p 49160:22 -p 49161:1521 -e ORACLE_ALLOW_REMOTE=true wnameless/oracle-xe-11g;
echo -e "Waiting for serveur startup...";
sleep 0;
echo -e "\033[1;34mConnect to the server with SSH. Default pass: admin\033[0m";
ssh root@localhost -p 49160;
while [ $? -ne 0 ]
do
    ssh root@localhost -p 49160; 2>/dev/null;
    $? = $?;
done

echo "LOOOOOOOOOOL";
