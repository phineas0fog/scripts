#!/bin/zsh

#convert time
m=$(echo $1 | cut -d ":" -f 1);
s=$(echo $1 | cut -d ":" -f 2);

time=$(($m*60 + $s));

echo "Waiting " $time "seconds..." ;

sleep $time;

echo "... end !";

#use msi keyboard led
if [ -f ~/Scripts/msiNotify.sh ]
then
    ~/Scripts/msiNotify.sh -1;
fi
