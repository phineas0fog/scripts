#!/bin/bash

address=localhost;

for port in $(seq 1 65535 )
do
    echo "Testing port  #$port";
    socat STDIO TCP:$address:$port 2>/dev/null && echo "Port $port is open";
    if [ $? -eq 0 ]
    then
	echo $port >> out.txt;
    fi
done
