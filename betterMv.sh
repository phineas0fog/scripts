#!/bin/zsh

#get owner of current dir
dirOwner=$(stat -c %U .);

#get current user
user=$(whoami);

if [ $dirOwner != $user ]
then
    echo -e '\033[1;31m Using sudo ? (y/n) \033[0m';
    read yn;
    case $yn in
	[Yy]*) sudo mv $*; ;;
	[Nn]*) echo -e '\033[1;31m Aborted ! \033[0m'; exit 1; ;;
	* ) echo -e '\033[1;31m Error, exit. \033[0m'; exit 1; ;;
    esac
else
    mv $*;
fi

