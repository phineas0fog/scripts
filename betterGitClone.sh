#!/bin/bash


## Colors
RED="\033[1;31m";
GREEN="\033[1;32m";
BLUE="\033[1;36m";
BOLD="\033[1m";
NOR="\033[0m";


if [ $# -eq 0 ]
then
    echo -e $BLUE"Clonning from clipboard"$NOR;
    gitUrl=$(xclip -o)
else
    gitUrl="$1";
fi

directory="$(echo $gitUrl | rev | cut -d '/' -f 1 | rev | cut -d '.' -f 1)";

if [ -d $directory ]
then
    echo -e $BLUE"Directory already exists... Remove ? [Y/n]"$NOR;
    read yn;
    case $yn in
	nN )
	    echo -e $RED"Exiting...";
	    exit 42;
	    ;;
	* )
	    echo -e $BLUE"Removing old directory..."$NOR;
	    rm -rf $directory;
	    if [ $? -eq 0 ]
	    then
		echo -e $GREEN"Done !"$NOR;
	    else
		echo -e $RED"Error... Exiting"$NOR;
		exit 42;
	    fi
	    ;;
    esac
fi

echo -e $GREEN"Cloning $gitUrl to \"$directory\"..."$NOR;
echo "----------";
git clone $gitUrl;
retcode=$?;
echo "----------";

if [ $retcode -eq 0 ]
then
    echo -e $GREEN"Done !"$NOR;
    echo "Moving to \"$directory\"...";
    cd "$directory";
else
    echo -e $RED"Failed !"$NOR;
    exit 42;
fi

