#!/bin/python

# import libraries
import os
from scipy import *

# function to fill array with file content
def getSave(saveFile="./save") :
    itemArray=array([]) # init array
    if not os.path.isfile(saveFile) :
        return itemArray
    else :
        for line in open(saveFile, "r"):
            itemArray=append(itemArray, line.replace("\n", ""))
        return itemArray

# function to display array's content
def disp():
    print("[I]", achievement, "\n--------") # display achievement
    for tree in items : # parse array and
        print("-"+tree) # print each item

# saving function
def saveInFile(items, saveFile="./save"):
    save=open(saveFile, "w")
    print("[I]Saving...")
    for item in items:
        save.write(item+"\n")
    save.close()
    print("[I]Done !")
        
# set number of items
total=315

# ask user to set the number of items
total=input("Put the number of items wanted...")

# check if value isnt null
if total =="":
    total=315
else:
    total=int(total)
    
# some courtesy
print("Thanks !")

# call "getSave" func to try to fill array
items=getSave()

while 1 :
    newItem=input("--------\nAdd item :\n>>>") # read item from stdin
    if newItem == "LIST":
        print("[I]Displaying all items harvested\n")
        disp()
        print("[I]end of display.")
    elif newItem == "EXIT":
        saveInFile(items)
        print("[I]Exiting...")
        exit()
    elif newItem == "SAVE":
        saveInFile(items)
    if not any(newItem in items):
        if newItem != "LIST" and newItem != "EXIT" and newItem != "SAVE":
            items=append(items, newItem) # add item to array
    else :
        print("[E]Item already harvested !")
    harvested=len(items) # get array lenght
    achievement=total-harvested # calculate achievement
    items.sort() # sort array by alapha order
    
save.write("%s\n" % newItem) # save array in file
save.close() # close save file
