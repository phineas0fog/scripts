#!/bin/env sh

#check if buffer exist and create if not
if [ -e /tmp/buffer ]
then
    rm /tmp/buffer;
fi
touch /tmp/buffer;

#copy absolute paths in buffer
echo $# > /tmp/buffer;
for arg in $*
do
    echo $(pwd)"/"$arg >> /tmp/buffer;
done
