#!/bin/zsh


# check if the dependencies are presents
if [ ! -f /usr/bin/msi-keyboard ]
then
    echo "This script require the msi-keyboard package !";
    exit 1;
fi

# function to turn off led when stopping
function ctrl_c()
{
    msi-keyboard -m normal -r front-right,0,0,0;
    echo "*** Stopped by user ***";
    exit 0;
}
trap ctrl_c SIGINT  SIGTERM

while true
do
    # get cpu usage percent
    cpu=$(~/Scripts/cpuMeterMsi/cpuShell.sh);

    #turn off led
    msi-keyboard -m normal -r front-right,0,0,0;
    
    # display correct color
    if (($cpu > 0 && $cpu <= 10))
    then
	msi-keyboard -m normal -r front-right,0,0,0;
    elif (($cpu > 10 && $cpu <= 25))
    then
	msi-keyboard -m normal -r front-right,195,255,0;
    elif (($cpu > 25 && $cpu <= 50))
    then
	msi-keyboard -m normal -r front-right,255,255,0;
    elif (($cpu > 50 && $cpu <= 75))
    then
	msi-keyboard -m normal -r front-right,255,160,0;
    elif (($cpu > 75))
    then
	msi-keyboard -m normal -r front-right,255,0,0;
    fi
    sleep 1;
done
