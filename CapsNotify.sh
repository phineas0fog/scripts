#!/bin/zsh

for i in 1 2 3 4
do
    echo 1 > /sys/class/leds/input1::capslock/brightness
    sleep 0.1;
    echo 0 > /sys/class/leds/input1::capslock/brightness
    sleep 0.1;
done
